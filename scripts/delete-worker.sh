#!/bin/bash
WORKER_NAME="$1"
if [ -z $WORKER_NAME ]; then
    echo "Missing worker name." && exit 1
fi

DELETE_WORKER_URL="https://api.cloudflare.com/client/v4/accounts/$CF_ACCOUNT_ID/workers/scripts/$WORKER_NAME"
echo $DELETE_WORKER_URL

curl -X DELETE \
    -H"Authorization: Bearer $CF_API_TOKEN" \
    $DELETE_WORKER_URL

$(dirname $0)"/delete-dns-record.sh" "$WORKER_NAME.dev-cati-sistemas.com.br"