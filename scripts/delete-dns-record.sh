#!/bin/bash
RECORD_NAME="$1"
if [ -z $RECORD_NAME ]; then
    echo "Missing record name." && exit 1
fi

RESPONSE=$(curl -s -X GET \
    -H "Authorization: Bearer $CF_ZONE_API_TOKEN" \
    "https://api.cloudflare.com/client/v4/zones/$CF_ZONE_ID/dns_records"
)

RECORD_ID=$(
    echo $RESPONSE \
        | jq ".result[] | select( .name == \"$RECORD_NAME\") | .id" \
        | sed 's/"//g'
)

curl -s -X DELETE \
    -H "Authorization: Bearer $CF_ZONE_API_TOKEN" \
    "https://api.cloudflare.com/client/v4/zones/$CF_ZONE_ID/dns_records/$RECORD_ID"
