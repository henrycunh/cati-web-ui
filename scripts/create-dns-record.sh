#!/bin/bash
RECORD_NAME="$1"
if [ -z $RECORD_NAME ]; then
    echo "Missing record name." && exit 1
fi

RECORD_CONTENT="$2"
if [ -z $RECORD_CONTENT ]; then
    echo "Missing record content." && exit 1
fi

RESPONSE=$(curl -s -X POST \
    -H "Authorization: Bearer $CF_ZONE_API_TOKEN" -H 'Content-type: application/json'\
    "https://api.cloudflare.com/client/v4/zones/$CF_ZONE_ID/dns_records" \
    -d @- <<EOF
    {
        "type": "A",
        "name": "$RECORD_NAME-web-ui",
        "content": "$RECORD_CONTENT",
        "ttl": 1,
        "proxied": true
    }
EOF
)

echo $RESPONSE