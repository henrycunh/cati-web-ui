#!/bin/bash
WORKER_ENV="$1"
if [ -z $WORKER_ENV ]; then
    echo "Missing worker environment." && exit 1
fi

if [[ $* == *--custom* ]]; then
    # Backups the original CF Workers configuration file
    cp wrangler.toml wrangler.toml.bkp
    # Adds custom environment to copy of configuration file
    tee -a wrangler.toml << EOF
[env.$WORKER_ENV]
name = "$WORKER_ENV-web-ui"
route = "$WORKER_ENV-web-ui.dev-cati-sistemas.com.br/*"
workers_dev = false
EOF
    # Creates record in cloudflare
    $(dirname $0)"/create-dns-record.sh" $WORKER_ENV "192.0.2.1"
    # Publish the site to the worker
    npx wrangler publish --env $WORKER_ENV
fi



if [[ $* == *--custom* ]]; then
    # Reverts the backup
    mv wrangler.toml.bkp wrangler.toml
fi